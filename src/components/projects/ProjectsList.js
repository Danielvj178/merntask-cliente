import React, { useContext, useEffect } from 'react';
import Project from './Project';
import projectContext from '../../context/Projects/ProjectContext';
import AlertContext from '../../context/Alerts/AlertContext';
import { CSSTransition, TransitionGroup } from 'react-transition-group';

const ProjectsList = () => {
    // Obtener variables de context
    const projectsContext = useContext(projectContext);
    const { message, projects, getProjects } = projectsContext;

    const alertContext = useContext(AlertContext);
    const { alert, showAlert } = alertContext;

    useEffect(() => {
        // Si hay un error cuando carga el componente
        if (message) {
            showAlert(message.msg, message.category);
        }

        getProjects();
        //eslint-disable-next-line
    }, [message]);

    // Revisar si hay projectos
    if (projects.length === 0) return <p className="text-center">No hay proyectos, comienza creando uno</p>;

    return (
        <ul className="listado-proyectos">
            {alert ? (<div className={`alerta ${alert.category}`}>{alert.msg}</div>) : null}
            <TransitionGroup>
                {projects.map(project => (
                    <CSSTransition
                        key={project._id}
                        timeout={200}
                        classNames="proyecto"
                    >
                        <Project
                            project={project}
                        />
                    </CSSTransition>
                ))}
            </TransitionGroup>
        </ul >
    );
}

export default ProjectsList;