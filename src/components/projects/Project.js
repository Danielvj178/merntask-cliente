import React, { useContext, useEffect } from 'react';
import projectContext from '../../context/Projects/ProjectContext';
import TaskContext from '../../context/Tasks/TaskContext';
import AuthenticationContext from '../../context/Authentication/AuthenticationContext';
const Project = ({ project }) => {
    // Obtener variables de context
    const projectsContext = useContext(projectContext);
    const { selectProject } = projectsContext;

    const authenticationContext = useContext(AuthenticationContext);
    const { userAuth } = authenticationContext;

    // Obtener tareas de context
    const tasksContext = useContext(TaskContext);
    const { loadTasks } = tasksContext;

    useEffect(() => {
        userAuth();
        //eslint-disable-next-line
    }, [])

    // Ejecutar funciones
    const clickProject = (id) => {
        selectProject(id);
        loadTasks(id);
    }

    return (
        <li>
            <button
                type="button"
                className="btn btn-blank"
                onClick={() => clickProject(project._id)}
            >
                {project.name}
            </button>
        </li>
    );
}

export default Project;