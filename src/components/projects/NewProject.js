import React, { Fragment, useState, useContext } from 'react';
import projectContext from '../../context/Projects/ProjectContext';

const NewProject = () => {

    // Obtener state del formulario desde el context
    const projectsContext = useContext(projectContext);
    const { newProject, error, showForm, addProject, showError } = projectsContext;

    // Creación del state
    const [project, setProject] = useState({
        name: ''
    });

    // Destructuring del state
    const { name } = project;

    // Cuando el usuario crea un nuevo proyecto
    const onSubmitProject = e => {
        e.preventDefault();

        //Validar proyecto
        if (name === '') {
            showError();
            return;
        }

        // Agregar al state
        addProject(project);

        // Reiniciar formulario
        setProject({
            name: ''
        })
    }

    //Actualiza el state con el valor que se va digitando
    const onChangeProject = e => {
        setProject({
            ...project,
            [e.target.name]: e.target.value
        })
    }

    return (
        <Fragment>
            <button
                type="button"
                className="btn btn-block btn-primario"
                onClick={showForm}
            >
                Nuevo Proyecto
            </button>

            {
                newProject
                    ?
                    <form
                        className="formulario-nuevo-proyecto"
                        onSubmit={onSubmitProject}
                    >
                        <input
                            type="text"
                            className="input-text"
                            placeholder="Nombre Proyecto"
                            name="name"
                            value={name}
                            onChange={onChangeProject}
                        />

                        <input
                            type="submit"
                            className="btn btn-primario btn-block"
                            value="Agregar Proyecto"
                        />
                    </form>
                    :
                    null
            }
            {error ? <p className="mensaje error">El nombre del proyecto es obligatorio</p> : null}
        </Fragment>
    );
}

export default NewProject;