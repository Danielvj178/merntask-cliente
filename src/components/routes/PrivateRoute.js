import React, { useContext, useEffect } from 'react';
import { Route, Redirect } from 'react-router-dom';
import AuthenticationContext from '../../context/Authentication/AuthenticationContext';

const PrivateRoute = ({ component: Component, ...props }) => {
    const authenticationContext = useContext(AuthenticationContext);
    const { authenticate, loading, userAuth } = authenticationContext;

    useEffect(() => {
        userAuth();
        //eslint-disable-next-line
    }, []);

    // Proteger componente para evitar que se acceda a través de la URL
    return (
        // Parametro de loading es par evitar que se vea el login antes cuando e recarga la página
        <Route {...props} render={props => (!authenticate && !loading) ? (
            <Redirect to="/" />
        ) : (
                <Component {...props} />
            )} />
    );
}

export default PrivateRoute;