import React, { useState, useContext, useEffect } from 'react';
import { Link } from 'react-router-dom';
import AlertContext from '../../context/Alerts/AlertContext';
import AuthenticationContext from '../../context/Authentication/AuthenticationContext';

const NewAccount = (props) => {

    // Extraer valores del context
    const alertContext = useContext(AlertContext);
    const { alert, showAlert } = alertContext;

    const authenticationContext = useContext(AuthenticationContext);
    const { message, authenticate, registerUser } = authenticationContext;

    // Si el usuario se ha autenticado o ya existe en la BD
    useEffect(() => {
        if (authenticate) {
            props.history.push('/projects');
        } else if (message) {
            showAlert(message.msg, message.category);
        }
        //eslint-disable-next-line
    }, [message, authenticate, props.history]);

    const [user, setUser] = useState({
        email: '',
        password: '',
        name: '',
        confirm: ''
    });

    const { name, email, password, confirm } = user;

    const handleChange = e => {
        setUser({
            ...user,
            [e.target.name]: e.target.value
        })
    }

    const handleSubmit = e => {
        e.preventDefault();

        // Validar que no haya campos vacíos
        if (name.trim() === '' || password.trim() === '' || email.trim() === '' || confirm.trim() === '') {
            showAlert('Todos los campos son obligatorios', 'alerta-error');
            return false;
        }

        // Password mínimo de 6 caracteres
        if (password.length < 6) {
            showAlert('El password debe tener al menos 6 caracteres', 'alerta-error');
            return false;
        }

        // Comprobar que las 2 contraseñas sean iguales
        if (password !== confirm) {
            showAlert('Las password no coinciden', 'alerta-error');
            return false;
        }

        registerUser({
            name,
            email,
            password
        })
    }

    return (
        <div className="form-usuario">
            {
                alert ? (<div className={`alerta ${alert.category}`}>{alert.msg}</div>) : null
            }
            <div className="contenedor-form sombra-dark">
                <h1>Obtener una cuenta</h1>

                <form
                    onSubmit={handleSubmit}
                >
                    <div className="campo-form">
                        <label htmlFor="name">Nombre</label>
                        <input
                            type="text"
                            id="name"
                            name="name"
                            placeholder="Tú Nombre"
                            value={name}
                            onChange={handleChange}
                        />
                    </div>

                    <div className="campo-form">
                        <label htmlFor="email">Email</label>
                        <input
                            type="email"
                            id="email"
                            name="email"
                            placeholder="Tú Email"
                            value={email}
                            onChange={handleChange}
                        />
                    </div>

                    <div className="campo-form">
                        <label htmlFor="password">Password</label>
                        <input
                            type="password"
                            id="password"
                            name="password"
                            placeholder="Tú Password"
                            value={password}
                            onChange={handleChange}
                        />
                    </div>

                    <div className="campo-form">
                        <label htmlFor="confirm">Confirmar Password</label>
                        <input
                            type="password"
                            id="confirm"
                            name="confirm"
                            placeholder="Tú Password"
                            value={confirm}
                            onChange={handleChange}
                        />
                    </div>

                    <div className="campo-form">
                        <input type="submit" className="btn btn-primario btn-block" value="Registrarme" />
                    </div>
                </form>
                <Link to={'/'} className="enlace-cuenta">Volver a Iniciar Sesión</Link>
            </div>
        </div>
    );
}

export default NewAccount;