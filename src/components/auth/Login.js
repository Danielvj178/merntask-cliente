import React, { useState, useContext, useEffect } from 'react';
import { Link } from 'react-router-dom';
import AuthenticationContext from '../../context/Authentication/AuthenticationContext';
import AlertContext from '../../context/Alerts/AlertContext';

const Login = (props) => {
    // Extraer valores del context
    const alertContext = useContext(AlertContext);
    const { alert, showAlert } = alertContext;

    const authenticationContext = useContext(AuthenticationContext);
    const { authenticate, message, login } = authenticationContext;

    const [user, setUser] = useState({
        email: '',
        password: ''
    });

    const { email, password } = user;

    // Si el usuario se ha autenticado o ya existe en la BD
    useEffect(() => {
        if (authenticate) {
            props.history.push('/projects');
        } else if (message) {
            showAlert(message.msg, message.category);
        }
        //eslint-disable-next-line
    }, [message, authenticate, props.history]);

    const handleChange = e => {
        setUser({
            ...user,
            [e.target.name]: e.target.value
        })
    }

    const handleSubmit = e => {
        e.preventDefault();

        if (email.trim() === '' || password.trim() === '') {
            showAlert('El usuario y la contraseña son obligatorios', 'alerta-error');
            return false;
        }

        login({
            email,
            password
        }
        );
    }

    return (
        <div className="form-usuario">
            {
                alert ? (<div className={`alerta ${alert.category}`}>{alert.msg}</div>) : null
            }
            <div className="contenedor-form sombra-dark">
                <h1>Iniciar Sesión</h1>

                <form
                    onSubmit={handleSubmit}
                >
                    <div className="campo-form">
                        <label htmlFor="email">Email</label>
                        <input
                            type="email"
                            id="email"
                            name="email"
                            placeholder="Tú Email"
                            value={email}
                            onChange={handleChange}
                        />
                    </div>

                    <div className="campo-form">
                        <label htmlFor="password">Password</label>
                        <input
                            type="password"
                            id="password"
                            name="password"
                            placeholder="Tú Password"
                            value={password}
                            onChange={handleChange}
                        />
                    </div>

                    <div className="campo-form">
                        <input type="submit" className="btn btn-primario btn-block" value="Iniciar Sesión" />
                    </div>
                </form>
                <Link to={'/new-account'} className="enlace-cuenta">Obtener cuenta</Link>
            </div>
        </div>
    );
}

export default Login;