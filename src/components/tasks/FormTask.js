import React, { useContext, Fragment, useState, useEffect } from 'react';
import projectContext from '../../context/Projects/ProjectContext';
import TaskContext from '../../context/Tasks/TaskContext';

const FormTask = () => {
    // Creación de state para tener los valores del form
    const [task, setTask] = useState({
        name: '',
        projectId: ''
    });

    // Destructuring del state
    const { name } = task;

    // Obtener variables de context
    const projectsContext = useContext(projectContext);
    const { project } = projectsContext;

    // Obtener variables y funciones del Context
    const taskContext = useContext(TaskContext);
    const { taskSelected, addTask, error, showError, loadTasks, updateTask } = taskContext;

    useEffect(() => {
        if (taskSelected !== null) {
            setTask(taskSelected);
        } else {
            setTask({
                name: '',
                projectId: ''
            })
        }

    }, [taskSelected]);

    if (!project)
        return null;

    // Obtener proyecto del array
    const [projectActual] = project;

    // Cuando el usuario crea un nuevo proyecto
    const onSubmitTask = e => {
        e.preventDefault();

        //Validar proyecto
        if (name.trim() === '') {
            showError();
            return;
        }

        // Validar si es edición o nueva tarea
        if (!taskSelected) {
            // Agregar al state
            task.project = projectActual._id;
            addTask(task);
        } else {
            updateTask(task);
        }

        // Cargar todas las tareas del proyecto
        loadTasks(projectActual._id);

        // Reiniciar formulario
        setTask({
            name: '',
            projectId: ''
        })
    }

    // Guardar valor de la tarea
    const onChangeTask = e => {

        setTask({
            ...task,
            [e.target.name]: e.target.value
        })
    }

    return (
        <Fragment>
            <div className="formulario">
                <form
                    onSubmit={onSubmitTask}
                >
                    <div className="contenedor-input">
                        <input
                            type="text"
                            className="input-text"
                            placeholder="Nombre Tarea"
                            name="name"
                            value={name}
                            onChange={onChangeTask}
                        />
                    </div>

                    <div className="contenedor-input">
                        <input
                            type="submit"
                            className="btn btn-primario btn-submit btn-block"
                            value={taskSelected ? 'Editar tarea' : 'Agregar Tarea'}
                        />
                    </div>
                </form>
            </div>
            {error ? <p className="mensaje error">El nombre de la tarea es obligatorio</p> : null}
        </Fragment>
    );
}

export default FormTask;