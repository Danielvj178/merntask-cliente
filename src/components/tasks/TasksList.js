import React, { useContext, Fragment } from 'react';
import Task from './Task';

import projectContext from '../../context/Projects/ProjectContext';
import TaskContext from '../../context/Tasks/TaskContext';
import { CSSTransition, TransitionGroup } from 'react-transition-group';

const TasksList = () => {

    // Obtener variables de context
    const projectsContext = useContext(projectContext);
    const { project, deleteProject } = projectsContext;

    // Obtener tareas de context
    const tasksContext = useContext(TaskContext);
    const { projectTasks } = tasksContext;

    // Validar que se seleccione un proyecto
    if (!project)
        return <h2>Selecciona un proyecto</h2>

    // Se aplica Array Destructuring
    const [projectActual] = project;

    const projectDelete = () => {
        deleteProject(projectActual._id)
    }

    return (
        <Fragment>
            {
                project
                    ?
                    <Fragment>
                        <h2>Proyecto: {projectActual.name}</h2>
                        <ul className="listado-tareas">
                            {projectTasks.length === 0
                                ? (<li className="tarea"><p>No hay tareas</p></li>)
                                :
                                <TransitionGroup>
                                    {projectTasks.map(task => (
                                        <CSSTransition
                                            key={task._id}
                                            timeout={200}
                                            classNames="tarea"
                                        >
                                            <Task
                                                task={task}
                                            />
                                        </CSSTransition>
                                    ))}
                                </TransitionGroup>
                            }
                        </ul>
                        <button
                            type="button"
                            className="btn btn-eliminar"
                            onClick={projectDelete}
                        >
                            Eliminar Proyecto &times;
                        </button>
                    </Fragment>
                    : null
            }
        </Fragment>
    );
}

export default TasksList;