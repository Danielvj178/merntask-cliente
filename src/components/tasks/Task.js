import React, { useContext } from 'react';
import TaskContext from '../../context/Tasks/TaskContext'

const Task = ({ task }) => {
    // Se obtienen las funciones del Context
    const taskContext = useContext(TaskContext);
    const { deleteTask, updateTask, saveTaskActual } = taskContext;

    const handleChangeDelete = taskId => {
        console.log(task);
        // Eliminar tarea
        deleteTask(taskId, task.project);
    }

    const handleChangeStatus = task => {
        task.status = (task.status) ? false : true;
        updateTask(task);
    }

    const handleSelectTask = task => {
        saveTaskActual(task);
    }

    return (
        <li className="tarea sombra">
            <p>{task.name}</p>

            <div className="estado">
                {task.status
                    ?
                    (
                        <button
                            type="button"
                            className="completo"
                            onClick={() => handleChangeStatus(task)}
                        >
                            Completo
                        </button>
                    )
                    :
                    (
                        <button
                            type="button"
                            className="incompleto"
                            onClick={() => handleChangeStatus(task)}
                        >
                            Incompleto
                        </button>
                    )
                }
            </div>

            <div className="acciones">
                <button
                    type="button"
                    className="btn btn-primario"
                    onClick={() => handleSelectTask(task)}
                >
                    Editar
                </button>

                <button
                    type="button"
                    className="btn btn-secundario"
                    onClick={() => handleChangeDelete(task._id)}
                >
                    Eliminar
                </button>
            </div>
        </li>
    );
}

export default Task;