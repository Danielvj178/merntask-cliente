import React, { useContext, useEffect } from 'react';
import AuthenticationContext from '../../context/Authentication/AuthenticationContext';

const Navbar = () => {
    // Extraer valores del context
    const authenticationContext = useContext(AuthenticationContext);
    const { user, userAuth, closeSession } = authenticationContext;

    useEffect(() => {
        userAuth();
        //eslint-disable-next-line
    }, [])

    return (
        < header className="app-header" >
            {(user) ? <p className="nombre-usuario">Hola <span>{user.name}</span></p> : null}
            <nav className="nav-principal">
                <button
                    className="btn btn-blank cerrar-sesion"
                    onClick={() => closeSession()}
                >Cerrar Sesión</button>
            </nav>
        </header >
    );
}

export default Navbar;