// Project
export const FORM_PROJECT = 'FORM_PROJECT';
export const GET_PROJECT = 'GET_PROJECT';
export const ADD_PROJECT = 'ADD_PROJECT';
export const VALIDATE_FORM = 'VALIDATE_FORM';
export const PROJECT_ACTUAL = 'PROJECT_ACTUAL';
export const DELETE_PROJECT = 'DELETE_PROJECT';
export const ERROR_PROJECT_API = 'ERROR_PROJECT_API';

// Task
export const TASKS_PROJECT = 'TASKS_PROJECT';
export const ADD_TASK = 'ADD_TASK';
export const VALIDATE_TASK = 'VALIDATE_TASK';
export const DELETE_TASK = 'DELETE_TASK';
export const TASK_ACTUAL = 'TASK_ACTUAL';
export const UPDATE_TASK = 'UPDATE_TASK';

// Alerts
export const SHOW_ALERT = 'SHOW_ALERT';
export const HIDE_ALERT = 'HIDE_ALERT';

export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';
export const REGISTER_ERROR = 'REGISTER_ERROR';
export const GET_USER = 'GET_USER';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_ERROR = 'LOGIN_ERROR';
export const CLOSE_SESSION = 'CLOSE_SESSION';