import React, { useReducer } from 'react';

import projectContext from './ProjectContext';
import ProjectReducer from './ProjectReducer';
import clientAxios from '../../config/axios';
import { ERROR_PROJECT_API } from '../../types/index';

import {
    FORM_PROJECT,
    GET_PROJECT,
    ADD_PROJECT,
    VALIDATE_FORM,
    PROJECT_ACTUAL,
    DELETE_PROJECT
} from '../../types';


const ProjectState = props => {
    const initialState = {
        projects: [],
        newProject: false,
        error: false,
        project: null,
        message: null
    }

    // Dispatch para ejecutar las acciones
    const [state, dispatch] = useReducer(ProjectReducer, initialState);

    // Funciones para el CRUD
    // Mostrar el formulario de nuevo proyecto
    const showForm = () => {
        dispatch({
            type: FORM_PROJECT
        });
    }

    // Obtener los proyectos
    const getProjects = async () => {

        try {
            const response = await clientAxios.get('api/projects');
            const projects = response.data;

            dispatch({
                type: GET_PROJECT,
                payload: projects
            })
        } catch (error) {
            console.log(error);
        }
    }

    // Agregar proyecto
    // El payload siempre será el parámetro que se recibe en la función
    const addProject = async project => {
        try {
            const response = await clientAxios.post('api/projects', project);

            dispatch({
                type: ADD_PROJECT,
                payload: response.data
            });
        } catch (error) {
            dispatch({
                type: ERROR_PROJECT_API,
                payload: alert
            })
        }

    }

    // Validar Formulario
    const showError = () => {
        dispatch({
            type: VALIDATE_FORM
        });
    }

    // Seleccionar proyecto actual
    const selectProject = projectId => {
        dispatch({
            type: PROJECT_ACTUAL,
            payload: projectId
        });
    }

    // Borrar proyecto
    const deleteProject = async projectId => {
        const alert = {
            msg: 'Existe un error en el API',
            category: 'alerta-error'
        }
        try {
            await clientAxios.delete(`api/projects/${projectId}`);
            dispatch({
                type: DELETE_PROJECT,
                payload: projectId
            });
        } catch (error) {
            dispatch({
                type: ERROR_PROJECT_API,
                payload: alert
            })
        }

    }

    return (
        <projectContext.Provider
            value={{
                projects: state.projects,
                newProject: state.newProject,
                error: state.error,
                project: state.project,
                message: state.message,
                showForm,
                getProjects,
                addProject,
                showError,
                selectProject,
                deleteProject
            }}
        >
            {props.children}
        </projectContext.Provider>

    );
}

export default ProjectState;