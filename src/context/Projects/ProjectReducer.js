import { FORM_PROJECT, GET_PROJECT, ADD_PROJECT, VALIDATE_FORM, PROJECT_ACTUAL, DELETE_PROJECT, ERROR_PROJECT_API } from "../../types";

export default (state, action) => {
    switch (action.type) {
        case FORM_PROJECT:
            return {
                ...state,
                newProject: true
            }
        case GET_PROJECT:
            return {
                ...state,
                projects: action.payload
            }
        case ADD_PROJECT:
            return {
                ...state,
                projects: [...state.projects, action.payload],
                // Ocultar fomulario de nuevo proyecto
                newProject: false,
                // Oculta mensaje de error
                error: false
            }
        case VALIDATE_FORM:
            return {
                ...state,
                error: true
            }
        case PROJECT_ACTUAL:
            return {
                ...state,
                project: state.projects.filter(project => project._id === action.payload)
            }
        case DELETE_PROJECT:
            return {
                ...state,
                projects: state.projects.filter(project => project._id !== action.payload),
                project: null
            }
        case ERROR_PROJECT_API:
            return {
                ...state,
                message: action.payload
            }

        default:
            return state;
    }
}