import React, { useReducer } from 'react';
import TaskContext from './TaskContext';
import clientAxios from '../../config/axios';
import TaskReducer from './TaskReducer';
import { TASKS_PROJECT, ADD_TASK, VALIDATE_TASK, DELETE_TASK, TASK_ACTUAL, UPDATE_TASK } from '../../types';

const TaskState = props => {
    const initialState = {
        projectTasks: [],
        error: false,
        taskSelected: null
    }

    // Crear dispatch y state
    const [state, dispatch] = useReducer(TaskReducer, initialState);

    // Obtener tareas por proyecto
    const loadTasks = async project => {

        try {
            const response = await clientAxios.get('api/tasks', { params: { project } });
            dispatch({
                type: TASKS_PROJECT,
                payload: response.data.tasks
            })
        } catch (error) {
            console.log(error);
        }
    }

    // Agregar una nueva tarea
    const addTask = async (task) => {
        try {
            const response = await clientAxios.post('api/tasks', task);
            dispatch({
                type: ADD_TASK,
                payload: response.data
            })
        } catch (error) {
            console.log(error);
        }
    }

    // Validar nueva tarea
    const showError = () => {
        dispatch({
            type: VALIDATE_TASK
        });
    }

    // Eliminar Tarea
    const deleteTask = async (taskId, project) => {
        try {
            await clientAxios.delete(`api/tasks/${taskId}`, { params: { project } });
            dispatch({
                type: DELETE_TASK,
                payload: taskId
            });
        } catch (error) {
            console.log(error);
        }

    }

    // Comprobar tarea actual
    const saveTaskActual = task => {
        dispatch({
            type: TASK_ACTUAL,
            payload: task
        })
    }

    // Actualizar tarea actual
    const updateTask = async task => {
        try {
            const response = await clientAxios.put(`api/tasks/${task._id}`, task);
            dispatch({
                type: UPDATE_TASK,
                payload: response.data.task
            });
        } catch (error) {
            console.log(error);
        }
    }

    return (
        <TaskContext.Provider
            value={{
                // Variables
                projectTasks: state.projectTasks,
                error: state.error,
                taskSelected: state.taskSelected,
                // Funciones
                loadTasks,
                addTask,
                deleteTask,
                showError,
                saveTaskActual,
                updateTask
            }}
        >
            {props.children}
        </TaskContext.Provider>
    )
}

export default TaskState;