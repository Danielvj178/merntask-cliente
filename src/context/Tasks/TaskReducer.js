import { TASKS_PROJECT, ADD_TASK, VALIDATE_TASK, DELETE_TASK, TASK_ACTUAL, UPDATE_TASK } from "../../types";

export default (state, action) => {
    switch (action.type) {
        case TASKS_PROJECT:
            return {
                ...state,
                projectTasks: action.payload
            }
        case ADD_TASK:
            return {
                ...state,
                projectTasks: [action.payload, ...state.projectTasks],
                error: false
            }
        case VALIDATE_TASK:
            return {
                ...state,
                error: true
            }
        case DELETE_TASK:
            return {
                ...state,
                projectTasks: state.projectTasks.filter(task => task._id !== action.payload)
            }
        case UPDATE_TASK:
            return {
                ...state,
                projectTasks: state.projectTasks.map(task => (task._id === action.payload._id) ? action.payload : task),
                taskSelected: null,
                error: false
            }
        case TASK_ACTUAL:
            return {
                ...state,
                taskSelected: action.payload
            }
        default:
            return state;
    }

}