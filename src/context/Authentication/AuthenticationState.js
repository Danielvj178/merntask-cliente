import React, { useReducer } from 'react';
import AuthenticationContext from './AuthenticationContext';
import AuthenticationReducer from './AuthenticationReducer';
import clientAxios from '../../config/axios';
import tokenAuth from '../../config/tokenAuth';

import { REGISTER_SUCCESS, REGISTER_ERROR, GET_USER, LOGIN_SUCCESS, LOGIN_ERROR, CLOSE_SESSION } from '../../types';

const AuthenticationState = props => {
    const initialState = {
        token: localStorage.getItem('token'),
        authenticate: null,
        user: null,
        message: null,
        loading: true
    }

    const [state, dispatch] = useReducer(AuthenticationReducer, initialState);

    // Crear nuevo usuario
    const registerUser = async data => {
        try {
            const response = await clientAxios.post('/api/user', data);

            dispatch({
                type: REGISTER_SUCCESS,
                payload: response.data
            });

            userAuth();
        } catch (error) {
            //console.log(error.response.data.msg);
            const alert = {
                msg: error.response.data.msg,
                category: 'alerta-error'
            }

            dispatch({
                type: REGISTER_ERROR,
                payload: alert
            })
        }
    }

    // Iniciar sesión
    const login = async (data) => {
        try {
            const response = await clientAxios.post('api/auth', data);

            dispatch({
                type: LOGIN_SUCCESS,
                payload: response.data
            });

            userAuth();

        } catch (error) {
            const alert = {
                msg: error.response.data.msg,
                category: 'alerta-error'
            }

            dispatch({
                type: LOGIN_ERROR,
                payload: alert
            })
        }
    }

    // Retorna usuario logueado
    const userAuth = async () => {
        const token = localStorage.getItem('token');

        if (token) {
            tokenAuth(token);
        }

        try {
            const response = await clientAxios.get('api/auth');
            dispatch({
                type: GET_USER,
                payload: response.data
            })

        } catch (error) {
            dispatch({
                type: LOGIN_ERROR
            })
        }
    }

    // Cerrar sesión
    const closeSession = () => {
        dispatch({
            type: CLOSE_SESSION
        });
    }

    return (
        <AuthenticationContext.Provider
            value={{
                token: state.token,
                authenticate: state.authenticate,
                user: state.user,
                message: state.message,
                loading: state.loading,
                registerUser,
                login,
                userAuth,
                closeSession
            }}
        >
            {props.children}
        </AuthenticationContext.Provider>
    )
}

export default AuthenticationState;