import { REGISTER_SUCCESS, REGISTER_ERROR, GET_USER, LOGIN_SUCCESS, LOGIN_ERROR, CLOSE_SESSION } from '../../types';

export default (state, action) => {
    switch (action.type) {
        case LOGIN_SUCCESS:
        case REGISTER_SUCCESS:
            localStorage.setItem('token', action.payload.token);
            return {
                ...state,
                authenticate: true,
                message: null,
                loading: false
            }
        case LOGIN_ERROR:
        case REGISTER_ERROR:
        case CLOSE_SESSION:
            localStorage.removeItem('token');
            return {
                ...state,
                token: null,
                user: null,
                authenticate: null,
                message: action.payload,
                loading: false
            }
        case GET_USER:
            return {
                ...state,
                authenticate: true,
                user: action.payload.user,
                loading: false
            }

        default:
            return state;
    }
}