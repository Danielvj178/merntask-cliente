import React from 'react';
import Login from './components/auth/Login';
import NewAccount from './components/auth/NewAccount';
import Projects from './components/projects/Projects';

// Utilización de Context
import ProjectState from './context/Projects/ProjectState';
import TaskState from './context/Tasks/TaskState';
import AlertState from './context/Alerts/AlertState';
import AuthenticationState from './context/Authentication/AuthenticationState';
import tokenAuth from './config/tokenAuth';
import PrivateRoute from './components/routes/PrivateRoute';

// Utilización de Rutas
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

function App() {

    const token = localStorage.getItem('token');
    if (token) {
        tokenAuth(token);
    }

    return (
        <ProjectState>
            <TaskState>
                <AlertState>
                    <AuthenticationState>
                        <Router>
                            <Switch>
                                <Route exact path="/" component={Login}></Route>
                                <Route exact path="/new-account" component={NewAccount}></Route>
                                <PrivateRoute exact path="/projects" component={Projects}></PrivateRoute>
                            </Switch>
                        </Router>
                    </AuthenticationState>
                </AlertState>
            </TaskState>
        </ProjectState>
    );
}

export default App;
