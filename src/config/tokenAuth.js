import clientAxios from './axios';

const tokenAuth = token => {
    (token) ? clientAxios.defaults.headers.common['x-auth-token'] = token : delete clientAxios.defaults.headers.common['x-auth-token'];
}

export default tokenAuth;